/*static const char *colorname[] = {
[0]=       "#000000",
[8]=       "#343434",
[1]=       "#e92f2f",
[9]=       "#e92f2f",
[2]=       "#0ed839",
[10]=      "#0ed839",
[3]=       "#dddd13",
[11]=      "#dddd13",
[4]=       "#3b48e3",
[12]=      "#3b48e3",
[5]=       "#f996e2",
[13]=      "#f996e2",
[6]=       "#23edda",
[14]=      "#23edda",
[7]=       "#ababab",
[15]=      "#f9f9f9",
};*/
static const char *colorname[] = {
	"#000000",  /*  0: black    */
	"#c0392b",  /*  1: red      */
	"#27ae60",  /*  2: green    */
	"#f39c12",  /*  3: yellow   */
	"#2980b9",  /*  4: blue     */
	"#8e44ad",  /*  5: magenta  */
	"#16a085",  /*  6: cyan     */
	"#f1f1f1",  /*  7: white    */
	"#1b1b1b",  /*  8: brblack  */
	"#c0392b",  /*  9: brred    */
	"#2ecc71",  /* 10: brgreen  */
	"#f1c40f",  /* 11: bryellow */
	"#3498db",  /* 12: brblue   */
	"#8e44ad",  /* 13: brmagenta*/
	"#16a085",  /* 14: brcyan   */
	"#fbfbfb",  /* 15: brwhite  */
	"#f1f1f1",
	"#c0392b",
	"#27ae60",
	"#f39c12",
	"#2980b9",
	"#8e44ad",
	"#16a085",
	"#141414",
	"#fbfbfb",
	"#c0392b",
	"#2ecc71",
	"#f1c40f",
	"#3498db",
	"#8e44ad",
	"#16a085",
	"#1b1b1b",
};
